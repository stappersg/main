
bong, a ping wrapper.

Typical use case::

   bong hostname.rebooted.server && sleep 4 && ssh hostname.rebooted.server


Purpose of `bong` is reducing `ping` output::

 PING 192.0.2.47 (192.0.2.47) 56(84) bytes of data.
 64 bytes from 192.0.2.47: icmp_seq=1 ttl=64 time=0.071 ms
 64 bytes from 192.0.2.47: icmp_seq=2 ttl=64 time=0.047 ms
 64 bytes from 192.0.2.47: icmp_seq=3 ttl=64 time=0.048 ms
 64 bytes from 192.0.2.47: icmp_seq=4 ttl=64 time=0.060 ms
 64 bytes from 192.0.2.47: icmp_seq=5 ttl=64 time=0.052 ms
 64 bytes from 192.0.2.47: icmp_seq=6 ttl=64 time=0.058 ms
 64 bytes from 192.0.2.47: icmp_seq=7 ttl=64 time=0.058 ms
 64 bytes from 192.0.2.47: icmp_seq=8 ttl=64 time=0.057 ms
 ping: sendmsg: Network is unreachable
 ping: sendmsg: Network is unreachable
 ping: sendmsg: Network is unreachable
 ping: sendmsg: Network is unreachable
 ping: sendmsg: Network is unreachable
 ping: sendmsg: Network is unreachable
 64 bytes from 192.0.2.47: icmp_seq=15 ttl=64 time=0.029 ms
 64 bytes from 192.0.2.47: icmp_seq=16 ttl=64 time=0.056 ms
 64 bytes from 192.0.2.47: icmp_seq=17 ttl=64 time=0.023 ms
 64 bytes from 192.0.2.47: icmp_seq=18 ttl=64 time=0.058 ms
 64 bytes from 192.0.2.47: icmp_seq=19 ttl=64 time=0.061 ms

into::

 18:15:35 just_started 0:00:00
 PING 192.0.2.47 (192.0.2.47) 56(84) bytes of data.
 18:15:36 still_up 0:00:01
 18:15:44 down 0:00:08
 18:15:50 up_again 0:00:06


Other benefits
--------
* Wall clock time showing when it was started and state changed
* Automatic stop of `ping`
* Exit codes for long command lines, see typical use case example above


Known issue
----
A **single** missed *ping reply* is seen as an **entire** *reboot cycle*.
Workaround is running `bong` on the same LAN,
so you avoid long distance connections where network packets might get lost.


Status
---
`bong` is in **production**. Author uses it 4 out of 5 workdays. At both quick
booting virtual machines and real hardware machines that take more time
to reboot.
There are ideas for some changes, but they are low prioritory.
Your feedback on `bong` will trigger re-evaluation of those prioritories.


Install
---
Copy single file `bong` into your `$PATH`. It needs `/usr/bin/python3`.
