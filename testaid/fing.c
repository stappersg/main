/* fing, fakes ping output */
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#define NOTHING 0
#define REPLY 1
#define UNREACHABLE 2
#define ST_STARTED 0
#define ST_STILL_UP 1
#define ST_DOWN 2
#define ST_UP_AGAIN 3
volatile sig_atomic_t flag_sigint = 0;
volatile sig_atomic_t flag_sigquit = 0;

void my_sigint_handler(int sig){
  flag_sigint = 1;
}

void my_sigquit_handler(int sig){
  flag_sigquit = 1;
}

void output(int what, int sqn){
  switch(what){
    case NOTHING:
      ; /* empty statement */
      break;
    case REPLY:
      printf("0 byte from fing: icmp_seq=%d ttl=1 time=0.01 ms\n", sqn);
      break;
    case UNREACHABLE:
      printf("From fing (fake ping) icmp_seq=%d Host Unreachable\n", sqn);
      break;
    default:
      printf("Oops, printing sequence number %d anyway\n", sqn);
      break;
  }
  fflush(stdout);
}

int evaluate(int stage, int up, int gap, int d){
//	printf("DEBUG s %d up %d, gap %d, d %d\n", stage, up, gap, d);
  switch(stage) {
    case ST_STILL_UP:
    case ST_UP_AGAIN:
      return REPLY;
    case ST_DOWN:
      if(d < 0) {
        return UNREACHABLE;
      } else {
        return NOTHING;
      }
    default:
      return -1 ;
  }
}

int main( int argc, char *argv[]){
  signal(SIGINT, my_sigint_handler);
  signal(SIGQUIT, my_sigquit_handler);

  int up, gap;
  int what;
  int finish, rebooted;
  int delay = 12;
  int stage = ST_STARTED;
  int i = 1;
  int sqn = 1;

  printf("PING replacement 'fing', ");
  fflush(stdout);
  if( argc < 2){
      fprintf(stderr, "\nE: Missing parameter \"uNgN\"\n");
      fprintf(stderr, "I: u for \"up\" and g for \"gap\"\n");
      fprintf(stderr, "I: N being an integer\n");
      exit(1);
  }
  if(sscanf(argv[1],"u%dg%d", &up, &gap) != 2){
      fprintf(stderr, "\nE: Parameter should be 'uNgN'\n");
      fprintf(stderr, "I: u for \"up\" and g for \"gap\"\n");
      fprintf(stderr, "I: N being an integer\n");
      exit(1);
  }
  printf("up %d, gap %d.\n", up, gap);
  fflush(stdout);
  rebooted = up + gap ;
  finish = rebooted + 3 ;
  for(;;){
    if((stage == ST_STARTED) && (up > 0)) {
      stage = ST_STILL_UP;
    }
    if((stage == ST_STARTED) && (up < 1)) {
      stage = ST_DOWN;
      delay = 0;  // immediate output of unreachable
    }
    if((stage == ST_STILL_UP) && (up < 1)) {
      stage = ST_DOWN;
    }
    if(sqn > rebooted) {
      stage = ST_UP_AGAIN;
    }
    if(sqn > finish) {
      return 0 ; /* stop with exit code zero */
    }
    if(stage == ST_STILL_UP) {
      up-- ;
    }
    if(stage == ST_DOWN) {
      gap-- ;
      delay-- ;
    }
    what = evaluate(stage, up, gap, delay);
    output(what, sqn);
    sqn++;
    sleep(1);
    if(flag_sigint){
      printf(" Stopped at sequence number %d\n", sqn);
      fflush(stdout);
      break;
    }
    if(flag_sigquit){
      fprintf(stderr, "Signal SIGQUIT counter %d\n", i);
      i++;
      flag_sigquit = 0;
    }
  }
  return SIGINT;
}
// l l
