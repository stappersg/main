#
# makefile
#
all:
	@echo I: nothing to compile
	@echo I: ./bong

install:
	cp -p bong /usr/bin/

uninstall:
	rm -f /usr/bin/bong


png: states.png

states.png: states.dot
	# have Graphviz installed
	#  sudo apt install graphviz
	dot -Tpng $< > $@

clean:
	rm -f states.png
	rm -f testoutput.*

test:
	@echo a test run takes about two minutes
	testaid/run 2>&1 \
	| sed -e 's/^..:..:.. \(.*\)/HH:MM:SS \1/' \
	> testoutput.$(shell date +%T )

# l l
